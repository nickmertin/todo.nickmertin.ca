import * as $ from 'jquery'
import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'

import VueMaterial from 'vue-material'
import './assets/theme.scss'
import 'vue-material/dist/vue-material.min.css'

$.extend(window, { $ })

Vue.use(VueMaterial)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
