import config from './config'
import store from './store'
import { logout } from './router'
import deepmerge from 'deepmerge'
import Vue from 'vue'

export async function api(path, options = {}, ignoreToken = false) {
  try {
    return await $.ajax(config.apiBase + path, deepmerge(ignoreToken || store.state.authToken === null ? {} : {
      headers: {
        'X-Auth-Token': store.state.authToken
      }
    }, options))
  } catch (e) {
    if (e.status === 403 && path !== '/logout') {
      logout()
    } else {
      throw e
    }
  }
}

export function post(path, data, options = {}, ignoreToken = false) {
  return api(path, deepmerge({
    method: 'POST',
    contentType: 'application/json; charset=utf-8',
    data: typeof data === 'object' ? JSON.stringify(data) : data
  }, options), ignoreToken)
}

export function nextTick() {
  return new Promise(resolve => Vue.nextTick(resolve))
}

export function delay(time) {
  return new Promise(resolve => setTimeout(resolve, time))
}

export function sortBy(array, selector) {
  return array.sort((a, b) => {
    const _a = selector(a)
    const _b = selector(b)
    return (_a > _b) - (_a < _b)
  })
}
