import Vue from 'vue'
import Router from 'vue-router'
import { sync } from 'vuex-router-sync'
import store from './store'
import { post } from './utils'
import Home from './components/Home'
import Login from './components/Login'
import List from './components/List'
import NewProject from './components/NewProject'
import Project from './components/Project'
import Feeds from './components/Feeds'
import NewFeed from './components/NewFeed'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/list/:id',
      component: List
    },
    {
      path: '/new-project',
      component: NewProject
    },
    {
      path: '/project/:id',
      component: Project
    },
    {
      path: '/feeds',
      component: Feeds
    },
    {
      path: '/feeds/new',
      component: NewFeed
    }
  ]
})

function isLogin(route) {
  return route.path === '/login'
}

router.beforeEach((to, from, next) => {
  console.log(to)
  if (!isLogin(to) && !store.getters.authenticated) {
    next(`/login?next=${encodeURIComponent(to.fullPath)}`)
  } else {
    next()
  }
})

sync(store, router)

export default router

export function logout() {
  post('/logout', {})
  store.commit('updateToken', null)
  if (!isLogin(router.currentRoute)) {
    router.push(`/login?next=${encodeURIComponent(router.currentRoute.fullPath)}`)
  }
}
