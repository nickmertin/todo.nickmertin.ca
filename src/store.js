import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    authToken: sessionStorage.getItem('authToken'),
    projects: null
  },
  getters: {
    authenticated (state) {
      return state.authToken !== null
    }
  },
  mutations: {
    updateToken (state, token) {
      state.authToken = token
      if (token === null) {
        sessionStorage.removeItem('authToken')
      } else {
        sessionStorage.setItem('authToken', token)
      }
    },
    updateProjects (state, projects) {
      state.projects = projects
    }
  }
})

export default store
